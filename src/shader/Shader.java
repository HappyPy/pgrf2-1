package shader;

import geometry.Vertex;
import transform.Col;

public interface Shader {
    Col shade(Vertex vertex);
}
