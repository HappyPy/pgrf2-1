package shader;

import geometry.Vertex;
import transform.Col;

public class ConstantShader implements Shader{

    @Override
    public Col shade(Vertex vertex) {
        return vertex.getColor();
    }
}
