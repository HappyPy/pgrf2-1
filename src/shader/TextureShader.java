package shader;

import geometry.Vertex;
import transform.Col;
import transform.Vec2D;

import java.awt.image.BufferedImage;

public class TextureShader implements Shader {
    private BufferedImage texture;

    public TextureShader(BufferedImage texture) {
        this.texture = texture;
    }

    @Override
    public Col shade(Vertex vertex) {
        Vec2D uv = getTexel(vertex.getUv().mul(1/vertex.getOne()));
        return new Col(texture.getRGB((int) uv.getX(), (int) uv.getY()));
    }

    private Vec2D getTexel(Vec2D uv) {
        return uv.mul(new Vec2D(texture.getWidth() - 1, texture.getHeight() - 1));
    }
}
