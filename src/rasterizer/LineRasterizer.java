package rasterizer;

import geometry.Vertex;
import helper.RasterizerUtil;
import rastr.VisibilitityBuffer;
import transform.Vec3D;

public class LineRasterizer {
    public VisibilitityBuffer visibilitityBuffer;

    public LineRasterizer(VisibilitityBuffer visibilitityBuffer) {
        this.visibilitityBuffer = visibilitityBuffer;
    }

    public void drawLine(Vertex a, Vertex b) {
        a = a.dehomog();
        b = b.dehomog();

        Vec3D va = RasterizerUtil.viewportTransformation(a.getPosition().ignoreW(), visibilitityBuffer.getWidth(), visibilitityBuffer.getHeight());
        Vec3D vb = RasterizerUtil.viewportTransformation(b.getPosition().ignoreW(), visibilitityBuffer.getWidth(), visibilitityBuffer.getHeight());

        int x1 = (int) va.getX();
        int y1 = (int) va.getY();

        int x2 = (int) vb.getX();
        int y2 = (int) vb.getY();

        int d = 0;

        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(y2 - y1);

        int dx2 = 2 * dx;
        int dy2 = 2 * dy;

        int ix = x1 < x2 ? 1 : -1;
        int iy = y1 < y2 ? 1 : -1;

        int x = x1;
        int y = y1;

        if (dx >= dy) {
            while (true) {
                double t1 = (x - va.getX()) / (vb.getX() - va.getX());
                Vertex vertexAB = a.mul(1 - t1).add(b.mul(t1));
                visibilitityBuffer.drawPixel(x, y, vertexAB.getPosition().getZ(), vertexAB.getColor());
                if (x == x2)
                    break;
                x += ix;
                d += dy2;
                if (d > dx) {
                    y += iy;
                    d -= dx2;
                }
            }
        } else {
            while (true) {
                double t1 = (y - va.getY()) / (vb.getY() - va.getY());
                Vertex vertexAB = a.mul(1 - t1).add(b.mul(t1));
                visibilitityBuffer.drawPixel(x, y, vertexAB.getPosition().getZ(), vertexAB.getColor());
                if (y == y2)
                    break;
                y += iy;
                d += dx2;
                if (d > dy) {
                    x += ix;
                    d -= dy2;
                }
            }
        }
    }
}
