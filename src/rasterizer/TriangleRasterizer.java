package rasterizer;

import geometry.Vertex;
import helper.RasterizerUtil;
import rastr.VisibilitityBuffer;
import shader.Shader;
import transform.Vec2D;
import transform.Vec3D;

public class TriangleRasterizer {
    public VisibilitityBuffer visibilitityBuffer;

    public TriangleRasterizer(VisibilitityBuffer visibilitityBuffer) {
        this.visibilitityBuffer = visibilitityBuffer;
    }

    public void rasterizeTriangle(Vertex a, Vertex b, Vertex c, Shader shader) {
        // dehomog
        a = a.dehomog();
        b = b.dehomog();
        c = c.dehomog();

        // transformation to viewport
        Vec3D va = RasterizerUtil.viewportTransformation(a.getPosition().ignoreW(), visibilitityBuffer.getWidth(), visibilitityBuffer.getHeight());
        Vec3D vb = RasterizerUtil.viewportTransformation(b.getPosition().ignoreW(), visibilitityBuffer.getWidth(), visibilitityBuffer.getHeight());
        Vec3D vc = RasterizerUtil.viewportTransformation(c.getPosition().ignoreW(), visibilitityBuffer.getWidth(), visibilitityBuffer.getHeight());

        // sort va.y < vb.y < vc.y
        if (va.getY() > vc.getY()) {
            Vec3D temp = va;
            va = vc;
            vc = temp;

            Vertex vTemp = a;
            a = c;
            c = vTemp;
        }
        if (va.getY() > vb.getY()) {
            Vec3D temp = vb;
            vb = va;
            va = temp;

            Vertex vTemp = b;
            b = a;
            a = vTemp;
        }
        if (vb.getY() > vc.getY()) {
            Vec3D temp = vb;
            vb = vc;
            vc = temp;

            Vertex vTemp = b;
            b = c;
            c = vTemp;
        }
        // upper portion of triangle
        for (int y = (int) (va.getY() + 1); y < vb.getY(); y++) {
            double t1 = (y - va.getY()) / (vb.getY() - va.getY());
            double t2 = (y - va.getY()) / (vc.getY() - va.getY());

            Vec3D vAB = va.mul(1 - t1).add(vb.mul(t1));
            Vec3D vAC = va.mul(1 - t2).add(vc.mul(t2));

            Vertex vertexAB = a.mul(1 - t1).add(b.mul(t1));
            Vertex vertexAC = a.mul(1 - t2).add(c.mul(t2));

            if (vAB.getX() > vAC.getX()) {
                Vec3D temp = vAB;
                vAB = vAC;
                vAC = temp;

                Vertex vTemp = vertexAB;
                vertexAB = vertexAC;
                vertexAC = vTemp;
            }

            // draw pixel from left to right on x
            for (int x = (int) (vAB.getX() + 1); x < vAC.getX(); x++) {

                double s = (x - vAB.getX()) / (vAC.getX() - vAB.getX());
                double z = vAB.getZ() * (1.0 - s) + vAC.getZ() * s;

                Vertex vertexABC = vertexAB.mul(1.0 - s).add(vertexAC.mul(s));
                visibilitityBuffer.drawPixel(x, y, z, shader.shade(vertexABC));
            }
        }

        // bottom portion of triangle
        for (int y = (int) (vb.getY() + 1); y < vc.getY(); y++) {
            double t1 = (y - vb.getY()) / (vc.getY() - vb.getY());
            double t2 = (y - va.getY()) / (vc.getY() - va.getY());

            Vec3D vBC = vb.mul(1 - t1).add(vc.mul(t1));
            Vec3D vAC = va.mul(1 - t2).add(vc.mul(t2));

            Vertex vertexBC = b.mul(1 - t1).add(c.mul(t1));
            Vertex vertexAC = a.mul(1 - t2).add(c.mul(t2));


            if (vBC.getX() > vAC.getX()) {
                Vec3D temp = vBC;
                vBC = vAC;
                vAC = temp;

                Vertex vTemp = vertexBC;
                vertexBC = vertexAC;
                vertexAC = vTemp;
            }
            // draw pixel from left to right on x
            for (int x = (int) (vBC.getX() + 1); x < vAC.getX(); x++) {
                double s = (x - vBC.getX()) / (vAC.getX() - vBC.getX());
                double z = vBC.getZ() * (1.0 - s) + vAC.getZ() * s;

                Vertex vertexABC = vertexBC.mul(1.0 - s).add(vertexAC.mul(s));
                visibilitityBuffer.drawPixel(x, y, z, shader.shade(vertexABC));
            }

        }
    }
}
