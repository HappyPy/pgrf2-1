package rastr;

import transform.Col;

import java.awt.image.BufferedImage;

public class ImageBufferImpl implements Raster<Col> {
    private BufferedImage bufferedImage;

    public ImageBufferImpl(BufferedImage image){
        this.bufferedImage = image;
    }

    @Override
    public int getWidth() {
        return bufferedImage.getWidth();
    }

    @Override
    public int getHeight() {
        return bufferedImage.getHeight();
    }

    @Override
    public Col getElement(int x, int y) throws IndexOutOfBoundsException {
        return new Col(bufferedImage.getRGB(x, y));
    }

    @Override
    public void setElement(int x, int y, Col element) throws IndexOutOfBoundsException {
        bufferedImage.setRGB(x, y, element.getRGB());
    }

}
