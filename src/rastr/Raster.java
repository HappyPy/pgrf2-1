package rastr;

public interface Raster<T> {
    int getWidth();
    int getHeight();

    T getElement(int x, int y) throws IndexOutOfBoundsException;
    void setElement(int x, int y, T element) throws IndexOutOfBoundsException;

    default boolean isInBounds(int x, int y) {
        return (x >= 0 && y >= 0 && x < getWidth() && y < getHeight());
    }
}
