package rastr;

public class ZBufferImpl implements Raster<Double> {
    private final int width;
    private final int height;
    private double[][] buffer;

    public ZBufferImpl(int width, int height) {
        this.width = width;
        this.height = height;
        this.buffer = new double[width][height];
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public Double getElement(int x, int y) throws IndexOutOfBoundsException {
        if (!isInBounds(x,y)) {
            return null;
        }
        return buffer[x][y];
    }

    @Override
    public void setElement(int x, int y, Double element){
        if (isInBounds(x,y)) {
            buffer[x][y] = element;
        }
    }

}
