package rastr;

import transform.Col;

import java.awt.image.BufferedImage;

public class VisibilitityBuffer {
    private ImageBufferImpl imageBuffer;
    private ZBufferImpl zBuffer;
    private Col background;

    public VisibilitityBuffer(BufferedImage img) {
        this.imageBuffer = new ImageBufferImpl(img);
        this.zBuffer = new ZBufferImpl(img.getWidth(), img.getHeight());
    }

    public VisibilitityBuffer(int width, int height) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        this.imageBuffer = new ImageBufferImpl(img);
        this.zBuffer = new ZBufferImpl(img.getWidth(), img.getHeight());
    }

    public void drawPixel(int x, int y, double z, Col col) {
        if (zBuffer.getElement(x, y) != null && zBuffer.getElement(x, y) > z) {
            imageBuffer.setElement(x, y, col);
            zBuffer.setElement(x, y, z);
        }
    }

    public void setBackgroundColor(Col col) {
        this.background = col;
    }

    public void clear() {
        for (int x = 0; x < imageBuffer.getWidth() - 1; x++) {
            for (int y = 0; y < imageBuffer.getHeight() - 1; y++) {
                zBuffer.setElement(x, y, 1d);
                imageBuffer.setElement(x, y, background);
            }
        }
    }

    public int getWidth() {
        return imageBuffer.getWidth();
    }

    public int getHeight() {
        return imageBuffer.getHeight();
    }
}
