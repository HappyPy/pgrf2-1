package rastr;

import geometry.Vertex;
import transform.Mat4;

public interface Vectorable {
    Vertex mul(double d);
    Vertex mul(Mat4 m);
    Vertex add(Vertex v);
}
