package test;

import geometry.objects.Tetraheadron;
import rasterizer.LineRasterizer;
import rasterizer.TriangleRasterizer;
import rastr.VisibilitityBuffer;
import render.Renderer;
import shader.ConstantShader;
import transform.Col;

public class rendererTest {
    private static void triangleTest() {
        int width = 800;
        int height = 400;
        VisibilitityBuffer visibilitityBuffer = new VisibilitityBuffer(width, height);
        TriangleRasterizer tr = new TriangleRasterizer(visibilitityBuffer);
        LineRasterizer lr = new LineRasterizer(visibilitityBuffer);
        Renderer rn = new Renderer(tr, lr);
        Tetraheadron tetraheadron = new Tetraheadron();

        rn.draw(tetraheadron);
        System.out.println("Test done");
    }

    public static void main(String[] args) {
        triangleTest();
    }
}
