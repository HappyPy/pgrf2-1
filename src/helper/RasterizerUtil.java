package helper;

import transform.Point3D;
import transform.Vec3D;

public class RasterizerUtil {

    static public Vec3D viewportTransformation(Vec3D vec, int width, int height) {
        return vec.mul(new Vec3D(1,-1,1)).add(new Vec3D(1,1,0)).mul(new Vec3D((width - 1) / 2.0,(height - 1) / 2.0,1));
    }

    static public boolean fastCut(Point3D a) {
        return !(-a.getW() >= a.getX() || -a.getW() >= a.getY() || a.getX() >= a.getW() || a.getY() >= a.getW()
                || 0 >= a.getZ() || a.getZ() >= a.getW());
    }
}
