package render;

import geometry.Part;
import geometry.Solid;
import geometry.Vertex;
import rasterizer.LineRasterizer;
import rasterizer.TriangleRasterizer;
import shader.Shader;
import transform.Mat4;

import java.util.List;
import java.util.stream.Collectors;

import static helper.RasterizerUtil.fastCut;

public class Renderer {

    private Mat4 view;
    private Mat4 perspective;
    private TriangleRasterizer triangleRasterizer;
    private LineRasterizer lineRasterizer;
    private boolean wireframe = false;

    public Renderer(TriangleRasterizer triangleRasterizer, LineRasterizer lineRasterizer) {
        this.triangleRasterizer = triangleRasterizer;
        this.lineRasterizer = lineRasterizer;
    }

    public void draw(Solid solid) {

        Mat4 matMVP = solid.getModel().mul(view).mul(perspective);

        List<Vertex> newVertices = solid.getGeometry().getVertexBuffer().stream().map(vertex -> vertex.mul(matMVP)).collect(Collectors.toList());

        for (Part part : solid.getTopology().getPartBuffer()) {
            List<Integer> indexBuffer = solid.getTopology().getIndexBuffer();
            switch (part.getType()) {
                case TRIANGLE:
                    for (int i = 0; i < part.getCount(); i++) {
                        int realIndex = (i * 3) + part.getStartIndex();
                        drawTriangle(
                                newVertices.get(indexBuffer.get(realIndex)),
                                newVertices.get(indexBuffer.get(realIndex + 1)),
                                newVertices.get(indexBuffer.get(realIndex + 2)), part.getShader());
                    }
                    break;
                case LINE:
                    for (int i = 0; i < part.getCount(); i++) {
                        int realIndex = (i * 2) + part.getStartIndex();
                        drawLine(
                                newVertices.get(indexBuffer.get(realIndex)),
                                newVertices.get(indexBuffer.get(realIndex + 1)));
                    }
                    break;
            }
        }
    }

    private void drawLine(Vertex p1, Vertex p2) {
        if (fastCut(p1.getPosition()) && fastCut(p2.getPosition())) {
            lineRasterizer.drawLine(p1, p2);
        }
    }

    private void drawTriangle(Vertex p1, Vertex p2, Vertex p3, Shader shader) {
        // fast cut, will cut parts of objects but is efficient to do when rendering with CPU
        if (fastCut(p1.getPosition()) && fastCut(p2.getPosition()) && fastCut(p3.getPosition()) && shader != null) {

            // sort p1.z < p2.z < p3.z
            if (p1.getPosition().getZ() > p3.getPosition().getZ()) {
                Vertex help = p1;
                p1 = p3;
                p3 = help;
            }
            if (p1.getPosition().getZ() > p2.getPosition().getZ()) {
                Vertex help = p1;
                p1 = p2;
                p2 = help;
            }
            if (p2.getPosition().getZ() > p3.getPosition().getZ()) {
                Vertex help = p2;
                p2 = p3;
                p3 = help;
            }

            if (p1.getPosition().getZ() <= 0) {
                return;
            }

            if (p1.getPosition().getZ() > 0 && p2.getPosition().getZ() < 0 && p3.getPosition().getZ() < 0) {
                p2 = calculateTriangleCut(p1, p2);
                p3 = calculateTriangleCut(p1, p3);
                if (!wireframe) {
                    triangleRasterizer.rasterizeTriangle(p1, p2, p3, shader);
                } else {
                    lineRasterizer.drawLine(p1, p2);
                    lineRasterizer.drawLine(p1, p3);
                    lineRasterizer.drawLine(p2, p3);
                }
                return;
            }
            if (p1.getPosition().getZ() > 0 && p2.getPosition().getZ() > 0 && p3.getPosition().getZ() < 0) {
                Vertex pt2 = calculateTriangleCut(p2, p3);
                p3 = calculateTriangleCut(p1, p3);
                if (!wireframe) {
                    triangleRasterizer.rasterizeTriangle(p1, p2, pt2, shader);
                    triangleRasterizer.rasterizeTriangle(p1, pt2, p3, shader);
                } else {
                    lineRasterizer.drawLine(p1, p2);
                    lineRasterizer.drawLine(p1, pt2);
                    lineRasterizer.drawLine(p2, pt2);

                    lineRasterizer.drawLine(p1, pt2);
                    lineRasterizer.drawLine(p3, pt2);
                    lineRasterizer.drawLine(p1, p3);
                }

                return;
            }
            if (!wireframe) {
                triangleRasterizer.rasterizeTriangle(p1, p2, p3, shader);
            } else {
                lineRasterizer.drawLine(p1, p2);
                lineRasterizer.drawLine(p1, p3);
                lineRasterizer.drawLine(p2, p3);
            }
        }
    }

    private Vertex calculateTriangleCut(Vertex v1, Vertex v2) {
        double t = v1.getPosition().getZ() / (v1.getPosition().getZ() - v2.getPosition().getZ());
        return v1.mul(1 - t).add(v2.mul(t));
    }

    public void setView(Mat4 view) {
        this.view = view;
    }

    public void setPerspective(Mat4 perspective) {
        this.perspective = perspective;
    }

    public void toggleWireframe() {
        this.wireframe = !this.wireframe;
    }
}
