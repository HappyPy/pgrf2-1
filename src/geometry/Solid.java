package geometry;

import transform.Mat4;
import transform.Mat4Identity;

public class Solid {
    private Geometry geometry;
    private Topology topology;
    private Mat4 model;

    public Solid() {
        this.geometry = new Geometry();
        this.topology = new Topology();
        model = new Mat4Identity();
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Topology getTopology() {
        return topology;
    }

    public void setTopology(Topology topology) {
        this.topology = topology;
    }

    public Mat4 getModel() {
        return model;
    }

    public void setModel(Mat4 model) {
        this.model = model;
    }
}
