package geometry;

import java.util.ArrayList;
import java.util.List;

public class Geometry {
    private List<Vertex> vertexBuffer;

    public Geometry(List<Vertex> vertexBuffer) {
        this.vertexBuffer = vertexBuffer;
    }

    public Geometry() {
        this.vertexBuffer =  new ArrayList<>();
    }

    public List<Vertex> getVertexBuffer() {
        return vertexBuffer;
    }

    public void setVertexBuffer(List<Vertex> vertexBuffer) {
        this.vertexBuffer = vertexBuffer;
    }
}
