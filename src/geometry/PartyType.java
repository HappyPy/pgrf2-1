package geometry;

public enum PartyType {
    LINE, LINE_STRIP, TRIANGLE, TRIANGLE_STRIP
}
