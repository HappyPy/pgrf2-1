package geometry.objects;

import geometry.*;
import helper.ImageHelper;
import shader.ConstantShader;
import shader.TextureShader;
import transform.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Plane extends Solid {
    public Plane(double step) {
        super();
        generatePlane(step);
    }

    private void generatePlane(double step){
        ArrayList<Vertex> vertices = new ArrayList<Vertex>();
        ArrayList<Integer> indexes = new ArrayList<Integer>();
        Point3D[] points = new Point3D[16];
        int index = 0;
        for (int i = 0; i <= 3; i++) {
            for (int j = 0; j <= 3; j++) {
                points[index] = new Point3D(i, j, Math.random() * 3);
                index++;
            }
        }
        Bicubic bicubic = new Bicubic(Cubic.BEZIER, points);

        for (double u = 0f; u < 1; u = u + step) {
            for (double v = 0f; v < 1; v = v + step) {
                Point3D p = bicubic.compute(u, v);
                vertices.add(new Vertex(p, new Col(0.0,0.0,0.0), new Vec2D(p.getX() / 3, p.getY()/3)));
            }
        }

        int count = 0;
        double sqrt = Math.sqrt(vertices.size());
        for (int i = 0; i < vertices.size(); i++) {
            if ((i + 1) % sqrt != 0 && (i + sqrt) < vertices.size()) {
                indexes.add(i);
                indexes.add(i + 1);
                indexes.add((int) (i + sqrt));
                count += 1;
                if (i < vertices.size() - sqrt) {
                    indexes.add((int) (i+ 1 + sqrt));
                    indexes.add(i + 1);
                    indexes.add((int) (i + sqrt));
                    count += 1;
                }
            }
        }


        ArrayList<Part> parts = new ArrayList<Part>() {};
        parts.add(new Part(PartyType.TRIANGLE, 0, count, new TextureShader(ImageHelper.loadImage("src/textures/johnny.jpg"))));
        setTopology(new Topology(indexes, parts));
        setGeometry(new Geometry(vertices));
    }
}
