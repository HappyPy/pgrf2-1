package geometry.objects;

import geometry.*;
import helper.ImageHelper;
import shader.ConstantShader;
import shader.TextureShader;
import transform.Col;
import transform.Point3D;
import transform.Vec2D;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;

public class Tetraheadron extends Solid {
    public Tetraheadron() {
        ArrayList<Vertex> vertices = new ArrayList<Vertex>() {
            {
                add(new Vertex(new Point3D(0, 0, 0), new Col(0, 0, 255), new Vec2D(0.5, 0)));
                add(new Vertex(new Point3D(0, 1, 0), new Col(0, 255, 0), new Vec2D(0, 0.5)));
                add(new Vertex(new Point3D(1, 0, 0), new Col(255, 0, 0), new Vec2D(0.5, 0)));
                add(new Vertex(new Point3D(0, 0, 1), new Col(0, 0, 255), new Vec2D(0, 0)));
            }
        };
        setGeometry(new Geometry(vertices));
        Integer[] indexes = {
                0,
                1,
                2,

                2,
                1,
                3,

                3,
                0,
                1,

                0,
                2,
                3,
        };
        ArrayList<Part> parts = new ArrayList<Part>() {
            {
                add(new Part(PartyType.TRIANGLE, 0, 4, new ConstantShader()));
            }
        };
        setTopology(new Topology(Arrays.asList(indexes), parts));
    }
}
