package geometry.objects;

import geometry.*;
import helper.ImageHelper;
import shader.TextureShader;
import transform.Col;
import transform.Mat4Identity;
import transform.Point3D;
import transform.Vec2D;

import java.util.ArrayList;
import java.util.Arrays;

public class Cube extends Solid {
    public Cube() {
        ArrayList<Vertex> vertices = new ArrayList<Vertex>() {
            {
                add(new Vertex(new Point3D(0, 0, 0), new Col(0, 0, 0), new Vec2D(0.75, 0.5))); //0

                add(new Vertex(new Point3D(1, 0, 0), new Col(0, 0, 0), new Vec2D(0.5, 0.5)));
                add(new Vertex(new Point3D(0, 1, 0), new Col(0, 0, 0), new Vec2D(0.75, 0.75)));
                add(new Vertex(new Point3D(1, 1, 0), new Col(0, 0, 0), new Vec2D(0.5, 0.75))); // 3

                add(new Vertex(new Point3D(0, 0, 1), new Col(0, 0, 0), new Vec2D(1, 0.5)));
                add(new Vertex(new Point3D(0, 1, 1), new Col(0, 0, 0), new Vec2D(1, 0.75))); // 5

                add(new Vertex(new Point3D(1, 0, 1), new Col(0, 0, 0), new Vec2D(0.5, 0.25)));
                add(new Vertex(new Point3D(0, 0, 1), new Col(0, 0, 0), new Vec2D(0.75, 0.25))); // 7

                add(new Vertex(new Point3D(0, 1, 1), new Col(0, 0, 0), new Vec2D(0.75, 1)));
                add(new Vertex(new Point3D(1, 1, 1), new Col(0, 0, 0), new Vec2D(0.5, 1))); // 9

                add(new Vertex(new Point3D(1, 1, 1), new Col(0, 0, 0), new Vec2D(0.25, 0.75)));
                add(new Vertex(new Point3D(1, 0, 1), new Col(0, 0, 0), new Vec2D(0.25, 0.5))); // 11

                add(new Vertex(new Point3D(0, 1, 1), new Col(0, 0, 0), new Vec2D(0, 0.75)));
                add(new Vertex(new Point3D(0, 0, 1), new Col(0, 0, 0), new Vec2D(0, 0.5))); // 13
            }
        };
        setGeometry(new Geometry(vertices));
        Integer[] indexes = {
                0, 1, 2,
                1, 2, 3,

                0, 2, 5,
                0, 5, 4,

                0, 1, 6,
                0, 6, 7,

                2, 3, 8,
                3, 8, 9,

                3, 10, 11,
                3, 11, 1,

                11, 10, 12,
                11, 12, 13
        };
        ArrayList<Part> parts = new ArrayList<Part>() {
            {
                add(new Part(PartyType.TRIANGLE, 0, 12, new TextureShader(ImageHelper.loadImage("src/textures/cube.jpg"))));
            }
        };
        setModel(new Mat4Identity());
        setTopology(new Topology(Arrays.asList(indexes), parts));
    }
}
