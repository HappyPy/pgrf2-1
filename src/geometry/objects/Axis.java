package geometry.objects;

import geometry.*;
import shader.ConstantShader;
import transform.Col;
import transform.Point3D;
import transform.Vec2D;

import java.util.ArrayList;
import java.util.Arrays;

public class Axis extends Solid {
    public Axis() {
        ArrayList<Vertex> vertices = new ArrayList<Vertex>() {
            {
                add(new Vertex(new Point3D(0, 0, 0), new Col(0, 0, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(0, 1, 0), new Col(0, 0, 255), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(1, 0, 0), new Col(255, 0, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(0, 0, 1), new Col(0, 255, 0), new Vec2D(0, 0)));

                add(new Vertex(new Point3D(0.1, 0.1, 0.8), new Col(0, 255, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(0.1, -0.1, 0.8), new Col(0, 255, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(-0.1, -0.1, 0.8), new Col(0, 255, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(-0.1, 0.1, 0.8), new Col(0, 255, 0), new Vec2D(0, 0)));

                add(new Vertex(new Point3D(0.8, 0.1, 0.1), new Col(255, 0, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(0.8, -0.1, 0.1), new Col(255, 0, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(0.8, -0.1, -0.1), new Col(255, 0, 0), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(0.8, 0.1, -0.1), new Col(255, 0, 0), new Vec2D(0, 0)));

                add(new Vertex(new Point3D(0.1, 0.8, 0.1), new Col(0, 0, 255), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(0.1, 0.8, -0.1), new Col(0, 0, 255), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(-0.1, 0.8, -0.1), new Col(0, 0, 255), new Vec2D(0, 0)));
                add(new Vertex(new Point3D(-0.1, 0.8, 0.1), new Col(0, 0, 255), new Vec2D(0, 0)));
            }
        };
        setGeometry(new Geometry(vertices));
        Integer[] indexes = {
                0, 1,
                0, 2,
                0, 3,

                3, 4, 5,
                3, 5, 6,
                3, 6, 7,
                3, 7, 4,
                4, 5, 6,
                4, 7, 6,

                2,8,9,
                2,9,10,
                2,10,11,
                2,11,8,
                8,9,10,
                8,11,10,

                1,12,13,
                1,13,14,
                1,14,15,
                1,15,12,
                12,13,14,
                12,15,14,
        };
        ArrayList<Part> parts = new ArrayList<Part>() {
            {
                add(new Part(PartyType.LINE, 0, 3));
                add(new Part(PartyType.TRIANGLE, 6, 18, new ConstantShader()));
            }
        };
        setTopology(new Topology(Arrays.asList(indexes), parts));
    }
}
