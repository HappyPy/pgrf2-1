package geometry;

import rastr.Vectorable;
import transform.*;

public class Vertex implements Vectorable {
    private Point3D position;
    private Col color;
    private Vec2D uv;
    private double one;

    public Vertex(Point3D position, Col color, Vec2D uv) {
        this.position = position;
        this.color = color;
        this.uv = uv;
        this.one = 1.0;
    }

    public Vertex(Point3D position, Col color, Vec2D uv, double one) {
        this.position = position;
        this.color = color;
        this.uv = uv;
        this.one = one;
    }

    public Point3D getPosition() {
        return position;
    }

    public Col getColor() {
        return color;
    }

    public Vec2D getUv() {
        return uv;
    }

    public double getOne() {
        return one;
    }

    public Vertex dehomog() {
        return new Vertex(position.mul(1 / position.getW()), color, uv.mul(1 / this.position.getW()), this.one / this.position.getW());
    }

    @Override
    public Vertex mul(double d) {
        return new Vertex(position.mul(d), color.mul(d), uv.mul(d), one * d);
    }

    @Override
    public Vertex mul(Mat4 m) {
        return new Vertex(position.mul(m), color, uv, one);
    }

    @Override
    public Vertex add(Vertex v) {
        return new Vertex(position.add(v.getPosition()), color.add(v.getColor()), uv.add(v.getUv()), one + v.getOne());
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "position=" + position +
                ", color=" + color +
                ", uv=" + uv +
                '}';
    }
}
