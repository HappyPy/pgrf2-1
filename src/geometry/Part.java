package geometry;

import shader.Shader;

public class Part {
    private PartyType type;
    private int startIndex;
    private int count;
    private Shader shader;

    public Part(PartyType type, int startIndex, int count, Shader shader) {
        this.type = type;
        this.startIndex = startIndex;
        this.count = count;
        this.shader = shader;
    }

    public Part(PartyType type, int startIndex, int count) {
        this.type = type;
        this.startIndex = startIndex;
        this.count = count;
    }

    public PartyType getType() {
        return type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getCount() {
        return count;
    }

    public Shader getShader() {
        return shader;
    }
}
