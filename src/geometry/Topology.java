package geometry;

import java.util.ArrayList;
import java.util.List;

public class Topology {
    private List<Integer> indexBuffer;
    private List<Part> partBuffer;

    public Topology() {
        this.indexBuffer = new ArrayList<>();
        this.partBuffer = new ArrayList<>();
    }

    public Topology(List<Integer> indexBuffer, List<Part> partBuffer) {
        this.indexBuffer = indexBuffer;
        this.partBuffer = partBuffer;
    }

    public List<Integer> getIndexBuffer() {
        return indexBuffer;
    }

    public List<Part> getPartBuffer() {
        return partBuffer;
    }
}
