import geometry.objects.Axis;
import geometry.objects.Cube;
import geometry.objects.Plane;
import geometry.objects.Tetraheadron;
import rasterizer.LineRasterizer;
import rasterizer.TriangleRasterizer;
import rastr.ImageBufferImpl;
import rastr.VisibilitityBuffer;
import rastr.ZBufferImpl;
import render.Renderer;
import shader.ConstantShader;
import shader.Shader;
import shader.TextureShader;
import transform.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import static constants.Constants.*;

public class App {
    @FunctionalInterface
    interface KeyAction {
        void doAction();
    }

    BufferedImage img;
    VisibilitityBuffer visibilitityBuffer;
    private JPanel panel;
    private static final float cameraSpeed = 1f;
    private static final float scaleUp = 1.1f;
    private static final float scaleDown = 0.9f;
    private HashMap<Integer, KeyAction> keyDispatcher = new HashMap<>();
    private Camera camera = new Camera()
            .withPosition(new Vec3D(8, 3, 3))
            .withAzimuth(3.3)
            .withZenith(-0.35);
    private Mat4 perspective;
    private boolean orthogonal = false;
    private Col backgroundColor = new Col(0x2f2f2f);
    private Renderer renderer;
    private Tetraheadron tetraheadron;
    private Axis axis;
    private Cube cube;
    private Plane plane;

    public App(int width, int height) {
        JFrame frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        visibilitityBuffer = new VisibilitityBuffer(img);
        perspective = new Mat4PerspRH(0.7853981633974483D, ((float) this.img.getHeight() / (float) this.img.getWidth()), 0.01D, 100.0D);
        renderer = new Renderer(new TriangleRasterizer(visibilitityBuffer), new LineRasterizer(visibilitityBuffer));
        renderer.setPerspective(perspective);
        tetraheadron = new Tetraheadron();
        tetraheadron.setModel(new Mat4Transl(2, 2, 2));
        axis = new Axis();
        cube = new Cube();
        plane = new Plane(0.1);
        plane.setModel(new Mat4RotZ(Math.PI));
        cube.setModel(new Mat4Transl(0.3, 0.3, 0));
        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

        keyDispatcher.put(KeyEvent.VK_W, () -> camera = camera.forward(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_S, () -> camera = camera.backward(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_A, () -> camera = camera.left(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_D, () -> camera = camera.right(cameraSpeed));
        keyDispatcher.put(KeyEvent.VK_O, () -> renderer.toggleWireframe());
        keyDispatcher.put(KeyEvent.VK_P, () -> {
            if (!orthogonal){
                perspective = new Mat4OrthoRH(5, 5, 0.1, 15);
            }else{
                perspective = new Mat4PerspRH(0.7853981633974483D, ((float) this.img.getHeight() / (float) this.img.getWidth()), 0.01D, 100.0D);
            }
            orthogonal = !orthogonal;
            renderer.setPerspective(perspective);
        });

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                try {
                    keyDispatcher.get(e.getKeyCode()).doAction();
                } catch (NullPointerException exception) {
                    System.out.println("That button doesn't do anything yet... " + e.getKeyCode());
                } finally {
                    draw();
                }
            }
        });
        frame.requestFocus();
        MouseAdapter mouseAdapter = new MouseAdapter() {
            int x, y;

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    x = e.getX();
                    y = e.getY();
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    int x1 = e.getX();
                    int y1 = e.getY();
                    float newA = (x1 - x) / 100f;
                    float newZ = (y1 - y) / 100f;
                    camera = camera.addAzimuth(newA);
                    camera = camera.addZenith(newZ);
                    x = x1;
                    y = y1;
                    draw();
                }
            }

        };
        frame.addMouseListener(mouseAdapter);
        frame.addMouseMotionListener(mouseAdapter);
    }

    public void draw() {
        visibilitityBuffer.clear();
        renderer.setView(camera.getViewMatrix());
        renderer.draw(tetraheadron);
        renderer.draw(axis);
        renderer.draw(cube);
        renderer.draw(plane);
        panel.repaint();
    }

    public void start() {
        visibilitityBuffer.setBackgroundColor(backgroundColor);
        draw();
        panel.repaint();
    }

    public void present(Graphics graphics) {
        graphics.drawImage(img, 0, 0, null);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new App(WIDTH, HEIGHT).start());
    }
}
